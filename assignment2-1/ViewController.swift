//
//  ViewController.swift
//  assignment2-1
//
//  Created by V.K. on 2/6/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // #0
        printMax(numberOne: 5, numberTwo: 3)
        
        // #1
        printSquaredAndCubed(number: 12.5)
        
        // #2
        printNumbersUpto(number: 12)
        
        // #3
        printAndCountDivisorsOf(number: 8128)
        
        // #4
        checkPerfect(number: 496)
        
    }

    func printMax(numberOne: Double, numberTwo: Double) {
        print("---------- 0 ----------")
        print("Input numbers are \(numberOne) and \(numberTwo)")
        var result: Double
        if numberOne > numberTwo {
            result = numberOne
        } else if numberOne < numberTwo {
            result = numberTwo
        } else {
            print("Numbers are equal.")
            return
        }
        print("Bigger number is \(result)")
    }
    
    func printSquaredAndCubed(number: Double) {
        print("---------- 1 ----------")
        print("Input number is \(number)")
        print("Number squared is \(number * number)")
        print("Number cubed is \(number * number * number)")
    }
    
    func printNumbersUpto(number: Int) {
        print("---------- 2 ----------")
        print("Input number is \(number)")
        assert(number > 0, "Invalid number: should be integer above 0.")
        
        var counterDown = number - 1
        for counterUp in 0..<number {
            print("\(counterUp)    \(counterDown)")
            counterDown -= 1
        }
    }
    
    func printAndCountDivisorsOf(number: Int) {
        print("---------- 3 ----------")
        print("Input number is \(number)")
        assert(number > 0, "Invalid number: should be integer above 0.")
        
        print(1)
        var divisorCount = 1
        let bound = Int(Double(number).squareRoot())
        for k in 2...bound {
            if number.isMultiple(of: k) {
                print(k)
                if k * k == number {
                    divisorCount += 1
                } else {
                    print(number / k)
                    divisorCount += 2
                }
            }
        }
        print("Total proper divisor count is \(divisorCount)")
    }
    
    func checkPerfect(number: Int) {
        print("---------- 4 ----------")
        print("Input number is \(number)")
        assert(number > 0, "Invalid number: should be integer above 0.")
        
        var divisorsSum = 1
        let bound = Int(Double(number).squareRoot())
        for k in 2...bound {
            if number.isMultiple(of: k) {
                divisorsSum += k
                if k * k != number {
                    divisorsSum += number / k
                }
            }
        }
        print("Total sum of proper divisors is \(divisorsSum)")
        print("\nThe number is\( number == divisorsSum ? "" : " not" ) perfect!")
    }
    
    
}

